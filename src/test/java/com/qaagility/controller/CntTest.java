package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class CntTest {
	
	@Test
	public void mulTest() throws Exception {
		Cnt c = new Cnt();
		assertEquals("Division",3,c.d(6,2));
	}

	@Test
	public void mulZeroTest() throws Exception {
		Cnt c = new Cnt();
		assertEquals("Division by Zero",Integer.MAX_VALUE, c.d(3,0));
		
	}

}

